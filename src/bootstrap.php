<?php
declare(strict_types=1);

use Nakima\CoreBundle\Exception\TODOException;

function TODO(string $message) {
    throw new TODOException($message);
}
