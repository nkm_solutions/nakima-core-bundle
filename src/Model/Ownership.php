<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Model;

interface Ownership
{
    public function getOwner();
}
