<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Model;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

class DataMapper
{

    protected $entity;
    protected $name;
    protected $fields;
    protected $type;

    public function __construct($entity)
    {
        $this->entity = $entity;
        $this->fields = [];
    }

    public function add($name, $type = null, $info = [])
    {
        $field = new DataMapperField();
        $field->setAttribute($name);
        $field->setLabel($info["label"] ?? $name);
        $field->setType($type);
        $this->fields[] = $field;

        return $this;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getName()
    {
        return $this->name;
    }
}