<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Model;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

class DataMapperField
{

    protected $attribute;
    protected $label;
    protected $value;
    protected $type;

    public function getAttribute()
    {
        return $this->attribute;
    }

    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}