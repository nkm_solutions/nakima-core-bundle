<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Utils;

use Nakima\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Symfony
{

    private static $container;

    public static function getRoot()
    {
        $dir = getcwd();

        //return preg_replace("/(.*)(\/web.*)/", "$1", $dir);

        return self::getKernel()->getRootDir()."/..";
    }

    public static function getContainer(): ContainerInterface
    {
        //if (!self::$container) {
        self::$container = self::getKernel()->getContainer();

        //}
        return self::$container;
    }

    public static function getDoctrine()
    {
        return self::get('doctrine');
    }

    public static function getTranslator()
    {
        return self::get('translator');
    }

    public static function getUser(): ?User
    {
        if (self::get('security.token_storage')->getToken()) {

            $ret = self::get('security.token_storage')->getToken()->getUser();
            if (is_string($ret)) {
                return null;
            }

            return $ret;
        }

        return null;
    }

    private static $kernel;

    public static function getKernel()
    {
        if (self::$kernel == null) {
            global $kernel;
            if ($kernel instanceOf \AppCache) {
                $kernel = $kernel->getKernel();
            }

            return $kernel;
        }

        return self::$kernel;
    }

    public static function setKernel($kernel)
    {
        self::$kernel = $kernel;
    }

    public static function get($service)
    {
        return self::getContainer()->get($service);
    }

    public static function isEnv($env): bool
    {
        return self::getContainer()->getParameter('kernel.environment') === $env;
    }

    public static function isEnvDev(): bool
    {
        return self::isEnv('dev');
    }

    public static function isEnvProd(): bool
    {
        return self::isEnv('prod');
    }

    public static function isEnvTest(): bool
    {
        return self::isEnv('test');
    }

    public static function checkEnv(string $env)
    {
        if (Symfony::isEnv($env)) {
            return;
        }

        throw new WrongEnvironmentException;
    }

    public static function checkNotEnv(string $env)
    {
        if (!Symfony::isEnv($env)) {
            return;
        }

        throw new WrongEnvironmentException;
    }
}
