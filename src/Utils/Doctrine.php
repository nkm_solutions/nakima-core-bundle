<?php
declare(strict_types=1);

namespace Nakima\CoreBundle\Utils;

use Doctrine\ORM\EntityRepository;

class Doctrine
{

    private static $container;
    private static $cache = [];

    public static function get()
    {
        return Symfony::getDoctrine();
    }


    public static function toArray($field, $options = [])
    {
        if ($field instanceof \Doctrine\ORM\PersistentCollection) {
            $ret = [];

            foreach ($field as $key => $value) {
                $ret[] = Doctrine::toArray($value, $options);
            }

            return $ret;
        }

        if ($field instanceof \Doctrine\Common\Collections\ArrayCollection) {
            $ret = [];

            foreach ($field as $key => $value) {
                $ret[] = Doctrine::toArray($value, $options);
            }

            return $ret;
        }

        if (is_array($field)) {
            $ret = [];

            foreach ($field as $key => $value) {
                $ret[] = Doctrine::toArray($value, $options);
            }

            return $ret;
        }

        if ($field instanceof \Nakima\CoreBundle\Entity\IEntity) {

            if ($field) {
                if (self::$cache[get_class($field) . "::" . $field->getId()] ?? false) {
                    return $field->getId();
                } else {
                    self::$cache[get_class($field) . "::" . $field->getId()] = true;
                    $ret = $field->__toArray($options);
                    self::$cache[get_class($field) . "::" . $field->getId()] = false;

                    return $ret;
                }
            }

            return null;
        }

        return $field;
    }

    public static function getId($field)
    {
        if ($field instanceof \Doctrine\ORM\PersistentCollection) {
            $ret = [];

            foreach ($field as $key => $value) {
                $ret[] = Doctrine::getId($value);
            }

            return $ret;
        }

        if ($field instanceof \Doctrine\Common\Collections\ArrayCollection) {
            $ret = [];

            foreach ($field as $key => $value) {
                $ret[] = Doctrine::getId($value);
            }

            return $ret;
        }

        if (is_array($field)) {
            $ret = [];

            foreach ($field as $key => $value) {
                $ret[] = Doctrine::getId($value);
            }

            return $ret;
        }

        if ($field instanceof \Nakima\CoreBundle\Entity\IEntity) {
            if ($field) {
                return $field->getId();
            }

            return null;
        }

        return $field;

    }

    public static function getRepo($fqn): EntityRepository
    {
        return self::get()->getRepository($fqn);
    }

    private static $manager;

    public static function loadManager()
    {
        self::$manager = self::get()->getManager();

        return self::$manager;
    }

    public static function flush()
    {
        self::loadManager();
        self::$manager->flush();
    }

    public static function persist($entity)
    {
        self::loadManager();
        self::$manager->persist($entity);
    }

    public static function remove($entity)
    {
        self::loadManager();
        self::$manager->remove($entity);
    }
}
