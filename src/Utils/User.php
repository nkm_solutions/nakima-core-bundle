<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Utils;

class User
{

    public function isActualUser($other)
    {
        $user = Symfony::getUser();

        return $user && $user->getid() == $other->getId();
    }
}
