<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\DependencyInjection;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder;
        $rootNode = $treeBuilder->root('nakima_core');

        $rootNode
            ->children()
                ->scalarNode('logo')
                    ->isRequired()
                ->end()
                ->scalarNode('small_logo')
                    ->isRequired()
                ->end()
                ->scalarNode('title')
                    ->isRequired()
                ->end()
                ->scalarNode('favicon')
                    ->isRequired()
                ->end()
                ->scalarNode('email')
                    ->isRequired()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }

}
