<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\DependencyInjection;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class NakimaCoreExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter('nakima', $config);

    }
}
