<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Controller\Injection;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;
use Nakima\CoreBundle\Entity\IEntity;
use Nakima\CoreBundle\Exception\AccessException;
use Nakima\CoreBundle\Exception\BadParamException;
use Nakima\CoreBundle\Exception\ManyMissingParamException;
use Nakima\CoreBundle\Exception\MethodException;
use Nakima\CoreBundle\Exception\MissingParamException;
use Nakima\CoreBundle\Exception\MustImplementException;
use Nakima\CoreBundle\Exception\ObjectNotFoundException;
use Nakima\CoreBundle\Exception\ResponseException;
use Nakima\CoreBundle\Model\Ownership;
use Nakima\CoreBundle\Security\Core\User\GroupInterface;
use Nakima\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait TraitBaseController
{
    public static $STRING = 0;
    public static $INTEGER = 1;
    public static $BOOLEAN = 2;
    public static $FLOAT = 3;
    public static $NATURAL = 4;
    public static $NATURAL_FLOAT = 5;
    public static $DATETIME = 6;
    public static $CHAR = 7;

    /* REQUEST */
    public static $ALL = 0;
    public static $ONE_OF_EACH = 1;
    public static $ONE = 2;
    private $missingParams = [];
    private $request;

    public function checkMethod(/* methods */)
    {
        $method = $this->getMethod();

        foreach (func_get_args() as $param) {
            if ($param === $method) {
                return $method;
            }
        }

        throw new MethodException($method);
    }

    public function getMethod(): string
    {
        return $this->optParam("_method", $this->getRequest()->getMethod());
    }

    /**
     * @param $name
     * @param null $default
     * @param int $type
     * @param null $obj
     * @param null $cb
     * @return string|bool|int|double|null
     */
    public function optParam($name, $default = null, $type = 0, $obj = null, $cb = null)
    {

        if ($this->isEnvProd() && $name[0] === "_" && $name[1] === "_") {
            if ($obj && $ret !== null) {
                call_user_func([$obj, $cb], $ret);
            }

            return $default;
        }

        $ret = $this->getRequest()->get($name, $default);

        if ($ret === "") {
            $ret = $default;
        }

        if ($obj && $ret !== null) {
            call_user_func([$obj, $cb], $ret);
        }

        return $ret;
    }

    public function getRequest(): Request
    {
        if (!$this->request) {
            $this->request = $this->container->get('request_stack')->getCurrentRequest();
        }

        return $this->request;
    }

    public function setRequest(Request $request): void
    {
        $this->request = $request;
    }

    public function isEnvProd(): bool
    {
        return $this->isEnv('prod');
    }

    public function isEnv($env): bool
    {
        return $this->container->getParameter('kernel.environment') === $env;
    }

    public function addBadParamIfTrue($condition, string $name): void
    {
        if ($condition) {
            $this->addBadParam($name);
        }
    }

    public function addBadParam(string $name): void
    {
        if (!in_array($name, $this->missingParams)) {
            $this->missingParams[] = $name;
        }
    }

    public function addBadParamIfFalse($condition, string $name): void
    {
        if (!$condition) {
            $this->addBadParam($name);
        }
    }

    public function checkMissingParams(): void
    {
        if (count($this->missingParams)) {
            throw new ManyMissingParamException($this->missingParams);
        }
    }

    public function getFile(string $file = null): ?UploadedFile
    {
        $ret = $this->optFile($file);
        if ($ret) {
            return $ret;
        }
        throw new MissingParamException($file ?? 'file');
        /*if ($theFile = $this->optFile($file)) {
            return $theFile;
        }

        throw new MissingParamException('file');*/
    }

    /**
     * @param null $file
     * @return null|UploadedFile
     */
    public function optFile(?string $file = null): ?UploadedFile
    {
        if ($file) {
            if ($file = $this->getRequest()->files->get($file)) {
                return $file;
            }

            return null;
        } else {
            foreach ($this->getRequest()->files->all() as $key => $value) {
                if ($value) {
                    return $value;
                }
            }

            return null;
        }
    }

    /* DOCTRINE */

    public function getParam(string $name, $error = true, $type = 0, $obj = null, $cb = null)
    {

        if ($error === null) {
            $error = true;
        }

        if (!$this->isEnvProd() && $name[0] === "_" && $name[1] === "_") {
            return null;
        }

        $ret = $this->optParam($name, null, $type);

        if ($error && $ret === null) {
            throw new MissingParamException($name);
        } else {
            if ($ret === null) {
                $this->missingParams[] = $name;
            }
        }

        if ($obj && $ret !== null) {
            call_user_func([$obj, $cb], $ret);
        }

        return $ret;
    }

    public function isDELETE(): bool
    {
        return $this->getMethod() === "DELETE";
    }

    /* SYMFONY */

    public function isGET(): bool
    {
        return $this->getMethod() === "GET";
    }

    public function isPOST(): bool
    {
        return $this->getMethod() === "POST";
    }

    public function isPUT(): bool
    {
        return $this->getMethod() === "PUT";
    }

    public function getRepo($fqn): EntityRepository
    {
        return $this->getDoctrine()->getRepository($fqn);
    }

    public function getDoctrine(): Registry
    {
        return parent::getDoctrine();
    }

    public function validate(IEntity $object, bool $crash = true)
    {
        $validator = $this->get('validator');

        $errors = $validator->validate($object);

        if (count($errors)) {
            if ($crash) {
                $field = $errors[0]->getPropertyPath();
                $message = $errors[0]->getMessage();
                throw new \Nakima\CoreBundle\Exception\BadParamException($field, $message);
            }
        }

        return $errors;
    }

    public function isEnvDev(): bool
    {
        return $this->isEnv('dev');
    }

    public function isEnvTest(): bool
    {
        return $this->isEnv('test');
    }

    /* SECURITY */

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function rawConnection(): \PDO
    {
        $host = $this->getParameter('database_host');
        $port = $this->getParameter('database_port');
        $db = $this->getParameter('database_name');
        $user = $this->getParameter('database_user');
        $pass = $this->getParameter('database_password');

        if ($port) {
            $conn = new \PDO("mysql:host=$host;dbname=$db;port=$port;charset=utf8", $user, $pass);
        } else {
            $conn = new \PDO("mysql:host=$host;dbname=$db;charset=utf8", $user, $pass);
        }

        return $conn;
    }

    public function checkRole(/* roles */): User
    {
        if (!call_user_func_array([$this, 'hasRole'], func_get_args())) {
            throw new AccessException();
        }

        return $this->getUser();
    }

    public function getUser(): ?User
    {
        $user = parent::getUser();

        if (!$user) {
            return null;
        }

        if (!$user->getEnabled()) {
            return null;
        }

        if ($user->getDeleted()) {
            return null;
        }

        foreach (func_get_args() as $class) {
            if (is_a($user, $class)) {
                return $user;
            }
        }

        return $user;
    }

    public function hasRole(/* $role */): bool
    {
        foreach (func_get_args() as $param) {
            if ($this->get('security.authorization_checker')->isGranted($param)) {
                return true;
            }
        }

        return false;
    }

    public function checkGroup(/* group */): User
    {
        if (!call_user_func_array([$this, 'hasGroup'], func_get_args())) {
            throw new AccessException();
        }

        return $this->getUser();
    }

    public function getUserProperties($roles = [], $classes = null, $groups = null, $filter = 2): User
    {
        $user = $this->hasUserProperties($roles, $classes, $groups, $filter);

        if (!$user) {
            throw new AccessException();
        }

        return $user;
    }

    public function hasUserProperties($roles = [], $classes = null, $groups = null, $filter = 2): bool
    {

        $user = parent::getUser();

        if (!$user) {
            return false;
        }

        //classes
        switch ($filter) {
            case self::ALL:
                if ($classes) {
                    $user = parent::getUser();

                    foreach ($classes as $class) {

                        if (!is_a($user, $class)) {
                            return false;
                        }
                    }
                }
                break;
            case self::ONE_OF_EACH:
                $found = false;
                if ($classes) {
                    $user = parent::getUser();

                    foreach ($classes as $class) {

                        if (is_a($user, $class)) {
                            $found = true;
                            break;
                        }
                    }
                }
                if (!$found) {
                    return false;
                }
                $found = false;
                break;
            case self::ONE:
                if ($classes) {
                    $user = parent::getUser();

                    foreach ($classes as $class) {

                        if (is_a($user, $class)) {
                            return $user;
                        }
                    }
                }
                break;
            default:
                return false;
        }

        switch ($filter) {
            case self::ALL:
                foreach ($roles as $role) {
                    if (!$this->get('security.authorization_checker')->isGranted($role)) {
                        return false;
                    }
                }
                break;
            case self::ONE_OF_EACH:
                $found = false;
                foreach ($roles as $role) {
                    if ($this->get('security.authorization_checker')->isGranted($role)) {
                        $found = true;
                    }
                }
                if (!$found) {
                    return false;
                }
                $found = false;
                break;
            case self::ONE:
                foreach ($roles as $role) {
                    if ($this->get('security.authorization_checker')->isGranted($role)) {
                        return true;
                    }
                }
                break;
            default:
                return false;
        }

        if ($groups) {
            switch ($filter) {
                case self::ALL:
                    foreach ($groups as $group) {
                        if (!$this->hasGroup($group)) {
                            return false;
                        }
                    }
                    break;
                case self::ONE_OF_EACH:
                    $found = false;
                    foreach ($groups as $group) {
                        if (!$this->hasGroup($group)) {
                            $found = true;
                        }
                    }
                    if (!$found) {
                        return false;
                    }
                    $found = false;
                    break;
                case self::ONE:
                    foreach ($groups as $group) {
                        if ($this->hasGroup($group)) {
                            return true;
                        }
                    }
                    break;
                default:
                    return false;
            }
        }

        return $user;
    }

    public function hasGroup(/* group */): bool
    {
        $groups = $this->checkUser(GroupInterface::class)->getGroups();

        foreach (func_get_args() as $param) {
            foreach ($groups as $group) {
                if ($user->hasGroup()) {
                    return true;
                }
            }
        }

        return false;
    }

    public function checkUser(?string $class = null): ?User
    {
        if (!call_user_func_array([$this, 'getUser'], func_get_args())) {
            throw new AccessException();
        }

        return parent::getUser();
    }

    public function checkOwnership($entity)
    {
        if (!$entity instanceof Ownership) {
            throw new MustImplementException($entity, Ownership::class);
        }

        if (!$this->getUser()) {
            throw new AccessException();
        }

        if ($entity->getOwner() != $this->getUser()) {
            throw new AccessException();
        }
    }

    public function checkAnonymous()
    {

        if ($this->getUser()) {
            throw new AccessException();
        }

        return $this->getuser();
    }

    // asserts

    public function assertTrue($in, int $st, string $message)
    {
        if (!$in) {
            throw new ResponseException(
                new JsonResponse(
                    [
                        'status'  => $st,
                        'message' => $message,
                    ], $st
                ), $message, $st
            );
        }

        return $this;
    }

    public function assertFalse($in, int $st, string $message)
    {
        if ($in) {
            throw new ResponseException(
                new JsonResponse(
                    [
                        'status'  => $st,
                        'message' => $message,
                    ], $st
                ), $message, $st
            );
        }

        return $this;
    }

    public function assertFalse400($item, string $param): self
    {
        if ($item) {
            throw new BadParamException($param);
        }

        return $this;
    }

    public function assertTrue400($item, string $param): self
    {
        if (!$item) {
            throw new BadParamException($param);
        }

        return $this;
    }

    /**
     * @param $item
     * @throws AccessException
     */
    public function assertTrue401($item): void
    {
        if (!$item) {
            throw new AccessException();
        }
    }

    public function assertFalse401($item)
    {
        if ($item) {
            throw new AccessException();
        }

        return $item;
    }

    public function assertTrue404($item, string $object, string $field, $value)
    {
        if (!$item) {
            throw new ObjectNotFoundException($object, $field, $value);
        }
    }

}
