<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Controller\Injection;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */


use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;
use Nakima\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait TraitBaseWebController
{

    public static $STRING = 0;
    public static $INTEGER = 1;
    public static $BOOLEAN = 2;
    public static $FLOAT = 3;
    public static $NATURAL = 4;
    public static $NATURAL_FLOAT = 5;
    public static $DATETIME = 6;
    public static $CHAR = 7;

    /**************************************************************************\
     *                                                                        *
     *   REQUEST                                                              *
     *                                                                        *
    \**************************************************************************/

    private $missingParams = [];
    private $request;

    public function checkMethod(/* methods */): string
    {
        $method = $this->getMethod();

        foreach (func_get_args() as $param) {
            if ($param === $method) {
                return $method;
            }
        }

        throw new MethodNotAllowedHttpException(func_get_args(), null, null, 405);
    }

    public function addBadParam(string $name): void
    {
        if (!in_array($name, $this->missingParams)) {
            $this->missingParams[] = $name;
        }
    }

    public function addBadParamIfTrue($condition, string $name): void
    {
        if ($condition) {
            $this->addBadParam($name);
        }
    }

    public function addBadParamIfFalse($condition, string $name): void
    {
        if (!$condition) {
            $this->addBadParam($name);
        }
    }

    public function checkMissingParams(): void
    {
        if (count($this->missingParams)) {
            throw new BadRequestHttpException(null, null, 400);
        }
    }

    public function getMethod(): string
    {
        return $this->optParam("_method", $this->getRequest()->getMethod());
    }

    public function getFile($file = null): UploadedFile
    {
        if ($theFile = $this->optFile($file)) {
            return $theFile;
        }

        throw new BadRequestHttpException(null, null, 400);
    }

    public function optFile($file = null): ?UploadedFile
    {
        if ($file) {
            if ($file = $this->getRequest()->files->get($file)) {
                return $file;
            }

            return false;
        } else {
            foreach ($this->getRequest()->files->all() as $key => $value) {
                return $value;
            }

            return false;
        }
    }

    public function getParam(string $name, bool $error = true, $type = 0, $obj = null, $cb = null)
    {

        if ($error === null) {
            $error = true;
        }

        if (!$this->isEnvProd() && $name[0] === "_" && $name[1] === "_") {
            return null;
        }

        $ret = $this->optParam($name, null, $type);

        if ($error && $ret === null) {
            throw new BadRequestHttpException(null, null, 400);
        } else {
            if ($ret === null) {
                $this->missingParams[] = $name;
            }
        }

        if ($obj && $ret !== null) {
            call_user_func([$obj, $cb], $ret);
        }

        return $ret;
    }

    public function optParam(string $name, $default = null, $type = 0, $obj = null, $cb = null)
    {

        if ($this->isEnvProd() && $name[0] === "_" && $name[1] === "_") {
            if ($obj && $ret !== null) {
                call_user_func([$obj, $cb], $ret);
            }

            return $default;
        }

        $ret = $this->getRequest()->get($name, $default);

        if ($ret === "") {
            $ret = $default;
        }

        if ($obj && $ret !== null) {
            call_user_func([$obj, $cb], $ret);
        }

        return $ret;
    }

    public function getRequest(): Request
    {
        if (!$this->request) {
            $this->request = $this->container->get('request_stack')->getCurrentRequest();
        }

        return $this->request;
    }

    public function isDELETE(): bool
    {
        return $this->getMethod() === "DELETE";
    }

    public function isGET(): bool
    {
        return $this->getMethod() === "GET";
    }

    public function isPOST(): bool
    {
        return $this->getMethod() === "POST";
    }

    public function isPUT(): bool
    {
        return $this->getMethod() === "PUT";
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**************************************************************************
     *                                                                        *
     *   DOCTRINE                                                             *
     *                                                                        *
     **************************************************************************/
    public function getRepo($fqn): EntityRepository
    {
        return $this->getDoctrine()->getRepository($fqn);
    }

    public function validate($object, $crash = true)
    {
        $validator = $this->get('validator');

        $errors = $validator->validate($object);
        if (count($errors)) {
            if ($crash) {
                $field = $errors[0]->getPropertyPath();
                $message = $errors[0]->getMessage();
                throw new BadRequestHttpException(null, null, 400);
            }
        }
        return $errors;
    }

    /**************************************************************************
     *                                                                        *
     *   SYMFONY                                                              *
     *                                                                        *
     **************************************************************************/
    public function isEnv($env): bool
    {
        return $this->container->getParameter('kernel.environment') === $env;
    }

    public function isEnvDev(): bool
    {
        return $this->container->getParameter('kernel.environment') === "dev";
    }

    public function isEnvProd(): bool
    {
        return $this->container->getParameter('kernel.environment') === "prod";
    }

    public function isEnvTest(): bool
    {
        return $this->container->getParameter('kernel.environment') === "test";
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function getDoctrine(): Registry
    {
        return parent::getDoctrine();
    }

    public function rawConnection(): \PDO
    {
        $host = $this->getParameter('database_host');
        $port = $this->getParameter('database_port');
        $db = $this->getParameter('database_name');
        $user = $this->getParameter('database_user');
        $pass = $this->getParameter('database_password');

        if ($port) {
            $conn = new \PDO("mysql:host=$host;dbname=$db;port=$port;charset=utf8", $user, $pass);
        } else {
            $conn = new \PDO("mysql:host=$host;dbname=$db;charset=utf8", $user, $pass);
        }

        return $conn;
    }

    /* SECURITY */
    public function checkRole(/* roles */): User
    {
        if (!call_user_func_array([$this, 'hasRole'], func_get_args())) {
            throw new AccessDeniedHttpException(null, null, 401);
        }

        return $this->getUser();
    }

    public function hasRole(/* $role */): bool
    {
        foreach (func_get_args() as $param) {
            if ($this->get('security.authorization_checker')->isGranted($param)) {
                return true;
            }
        }

        return false;
    }

    public function checkGroup(/* group */): User
    {
        if (!call_user_func_array([$this, 'hasGroup'], func_get_args())) {
            throw new AccessDeniedHttpException(null, null, 401);
        }

        return $this->getUser();
    }

    public function hasGroup(/* group */): bool
    {
        $groups = $this->checkUser(GroupInterface::class)->getGroups();

        foreach (func_get_args() as $param) {
            foreach ($groups as $group) {
                if ($user->hasGroup()) {
                    return true;
                }
            }
        }

        return false;
    }

    public static $ALL = 0;
    public static $ONE_OF_EACH = 1;
    public static $ONE = 2;

    public function getUserProperties($roles = [], $classes = null, $groups = null, $filter = 2): User
    {
        $user = $this->hasUserProperties($roles, $classes, $groups, $filter);

        if (!$user) {
            throw new AccessDeniedHttpException(null, null, 401);
        }

        return $user;
    }

    public function hasUserProperties($roles = [], $classes = null, $groups = null, $filter = 2): bool
    {

        $user = parent::getUser();

        if (!$user) {
            return false;
        }

        //classes
        switch ($filter) {
            case self::ALL:
                if ($classes) {
                    $user = parent::getUser();

                    foreach ($classes as $class) {

                        if (!is_a($user, $class)) {
                            return false;
                        }
                    }
                }
                break;
            case self::ONE_OF_EACH:
                $found = false;
                if ($classes) {
                    $user = parent::getUser();

                    foreach ($classes as $class) {

                        if (is_a($user, $class)) {
                            $found = true;
                            break;
                        }
                    }
                }
                if (!$found) {
                    return false;
                }
                $found = false;
                break;
            case self::ONE:
                if ($classes) {
                    $user = parent::getUser();

                    foreach ($classes as $class) {

                        if (is_a($user, $class)) {
                            return $user;
                        }
                    }
                }
                break;
            default:
                return false;
        }

        switch ($filter) {
            case self::ALL:
                foreach ($roles as $role) {
                    if (!$this->get('security.authorization_checker')->isGranted($role)) {
                        return false;
                    }
                }
                break;
            case self::ONE_OF_EACH:
                $found = false;
                foreach ($roles as $role) {
                    if ($this->get('security.authorization_checker')->isGranted($role)) {
                        $found = true;
                    }
                }
                if (!$found) {
                    return false;
                }
                $found = false;
                break;
            case self::ONE:
                foreach ($roles as $role) {
                    if ($this->get('security.authorization_checker')->isGranted($role)) {
                        return true;
                    }
                }
                break;
            default:
                return false;
        }

        if ($groups) {
            switch ($filter) {
                case self::ALL:
                    foreach ($groups as $group) {
                        if (!$this->hasGroup($group)) {
                            return false;
                        }
                    }
                    break;
                case self::ONE_OF_EACH:
                    $found = false;
                    foreach ($groups as $group) {
                        if (!$this->hasGroup($group)) {
                            $found = true;
                        }
                    }
                    if (!$found) {
                        return false;
                    }
                    $found = false;
                    break;
                case self::ONE:
                    foreach ($groups as $group) {
                        if ($this->hasGroup($group)) {
                            return true;
                        }
                    }
                    break;
                default:
                    return false;
            }
        }

        return $user;
    }

    public function checkOwnership($entity)
    {
        if (!$entity instanceof Ownership) {
            throw new BadRequestHttpException(null, null, 400);
        }

        if (!$this->getUser()) {
            throw new AccessDeniedHttpException(null, null, 401);
        }

        if ($entity->getOwner() != $this->getUser()) {
            throw new AccessDeniedHttpException(null, null, 401);
        }
    }

    public function checkUser($class = null): User
    {

        if (!call_user_func_array([$this, 'getUser'], func_get_args())) {
            throw new AccessDeniedHttpException(null, null, 401);
        }

        return parent::getUser();
    }

    public function checkAnonymous(): User
    {

        if ($this->getUser()) {
            throw new AccessDeniedHttpException(null, null, 401);
        }

        return $this->getUser();
    }

    public function getUser(): ?User
    {
        $user = parent::getUser();

        if (!$user) {
            return null;
        }

        foreach (func_get_args() as $class) {
            if (is_a($user, $class)) {
                return $user;
            }
        }

        return $user;
    }

    /**************************************************************************
     *                                                                        *
     *   ASSERTS                                                              *
     *                                                                        *
     **************************************************************************/

    public function assertFalse400($item): void
    {
        if ($item) {
            throw new BadRequestHttpException(null, null, 400);
        }
    }

    public function assertTrue400($item): void
    {
        if (!$item) {
            throw new BadRequestHttpException(null, null, 400);
        }

    }

    public function assertTrue401($item): void
    {
        if (!$item) {
            throw new AccessDeniedHttpException(null, null, 401);
        }

    }

    public function assertFalse401($item): void
    {
        if ($item) {
            throw new AccessDeniedHttpException(null, null, 401);
        }

    }

    public function assertTrue404($item): void
    {
        if (!$item) {
            throw new NotFoundHttpException(null, null, 404);
        }

    }

}
