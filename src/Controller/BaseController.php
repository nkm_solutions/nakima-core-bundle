<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Controller;

/**
 * @author xgonzalez
 */

use Nakima\CoreBundle\Controller\Injection\TraitBaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    use TraitBaseController;
}
