<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Controller;

/**
 * @author xgonzalez
 */

use Nakima\CoreBundle\Controller\Injection\TraitBaseWebController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseWebController extends Controller
{
    use TraitBaseWebController;
}

/*
renderView($view, array $parameters = Array)
renderView($view, array $parameters = Array)
*/
