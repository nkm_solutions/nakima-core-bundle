<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

/**
 * @author xgonzalez
 */

use Symfony\Component\HttpFoundation\JsonResponse;

class DbalException extends ResponseException
{

    public function __construct($field, $value, $hint = null, $message = null)
    {
        $response = new JsonResponse(
            [
                "status" => 400,
                "message" => $message ?? "Field '$field' cannot have '$value' value",
                "field" => $field,
                "value" => $value,
                "hint" => $hint ?? "",
            ],
            400
        );
        parent::__construct($response, $message);
    }

}
