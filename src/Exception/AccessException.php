<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

/**
 * @author xgonzalez
 */

use Symfony\Component\HttpFoundation\JsonResponse;

class AccessException extends ResponseException
{

    public function __construct()
    {
        $response = new JsonResponse(
            [
                "status" => 401,
                "message" => "Access denied",
            ],
            401
        );
        parent::__construct($response, "Access denied");
    }

}
