<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;

class MissingParamException extends ResponseException
{

    public function __construct($param)
    {

        $response = new JsonResponse(
            [
                "status" => 400,
                "message" => "Param '$param' is required",
                "param" => $param,
            ],
            400
        );

        parent::__construct($response, "Param '$param' is required");
    }
}
