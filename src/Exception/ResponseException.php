<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

/**
 * @author xgonzalez
 */

use Symfony\Component\HttpFoundation\Response;

class ResponseException extends \Exception
{

    private $response;
    private $status;

    public function __construct(Response $response, $message, $status = 500)
    {
        parent::__construct($message);
        $this->response = $response;
        $this->status = $status;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getStatus()
    {
        return $this->status;
    }
}
