<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;

class MethodException extends ResponseException
{

    public function __construct($method)
    {

        $response = new JsonResponse(
            [
                "status" => 405,
                "message" => "Method '$method' not allowed",
                "method" => $method,
            ],
            405
        );

        parent::__construct($response, "Method '$method' not allowed");
    }
}
