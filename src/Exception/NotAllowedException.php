<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

/**
 * @author xgonzalez
 */

use Symfony\Component\HttpFoundation\JsonResponse;

class NotAllowedException extends ResponseException
{

    public function __construct()
    {
        $response = new JsonResponse(
            [
                "status" => 401,
                "message" => "You cannot perform this action.",
            ],
            401
        );
        parent::__construct($response, "You cannot perform this action.");
    }

}
