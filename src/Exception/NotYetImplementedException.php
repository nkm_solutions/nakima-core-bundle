<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

class NotYetImplementedException extends \Exception
{

    public function __construct()
    {
        $bt = debug_backtrace(false, 2);
        $class = $bt[1]["class"];
        $function = $bt[1]["function"];
        $line = $bt[0]["line"];

        parent::__construct("Method $class\\$function: $line is not yet implemented.");
    }
}
