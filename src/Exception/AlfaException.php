<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

/**
 * @author xgonzalez
 */

use Symfony\Component\HttpFoundation\JsonResponse;

class AlfaException extends ResponseException
{

    public function __construct()
    {
        $response = new JsonResponse(
            [
                "status" => 0,
                "message" => "Nobody can stop being an alfa as nobody cannot convert into one.",
            ],
            0
        );
        parent::__construct($response, "Nobody can stop being an alfa as nobody cannot convert into one.");
    }

}
