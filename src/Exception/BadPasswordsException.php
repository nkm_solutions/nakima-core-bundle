<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;

class BadPasswordsException extends ResponseException
{

    public function __construct()
    {

        $response = new JsonResponse(
            [
                "status" => 400,
                "message" => "Passwords are not identical.",
            ],
            400
        );

        parent::__construct($response, "Passwords are not identical.");
    }
}
