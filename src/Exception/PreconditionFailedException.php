<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

/**
 * @author xgonzalez
 */

use Symfony\Component\HttpFoundation\JsonResponse;

class PreconditionFailedException extends ResponseException
{

    public function __construct()
    {
        $response = new JsonResponse(
            [
                "status" => 412,
                "message" => "Precondition failed",
            ],
            412
        );
        parent::__construct($response, "Precondition failed");
    }

}
