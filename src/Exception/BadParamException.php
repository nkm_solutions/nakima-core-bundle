<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;

class BadParamException extends ResponseException
{

    public function __construct($param, $message = null)
    {

        $response = new JsonResponse(
            [
                "status" => 400,
                "message" => $message ? $message : "Invalid field '$param'",
                "param" => $param,
            ],
            400
        );

        parent::__construct($response, "Invalid field '$param'");
    }
}
