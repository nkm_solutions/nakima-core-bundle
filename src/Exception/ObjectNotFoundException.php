<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;

class ObjectNotFoundException extends ResponseException
{

    public function __construct($class, $field, $value)
    {

        $response = new JsonResponse(
            [
                "status" => 404,
                "message" => "$class with $field $value does not exist.",
                "object" => $class,
                "field" => $field,
                "value" => $value,
            ],
            404
        );

        parent::__construct($response, "$class with $field $value does not exist.");
    }
}
