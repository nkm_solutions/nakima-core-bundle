<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;

class WrongEnvironmentException extends ResponseException
{

    public function __construct($param, $message = null)
    {

        $status = 500;
        $message = potset($message, "How dare you invoke me?");
        $response = new JsonResponse(
            [
                "status" => $status,
                "message" => $message,
            ],
            $status
        );

        parent::__construct($response, $message);
    }
}
