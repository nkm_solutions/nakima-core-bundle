<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;

class ManyMissingParamException extends ResponseException
{

    public function __construct($params)
    {

        $this->params = $params;
        $paramsString = join(", ", $params);

        if (count($params) > 1) {
            $message = "Parameters '$paramsString' are required";
        } else {
            $message = "Parameter '$paramsString' is required";
        }

        $response = new JsonResponse(
            [
                "status" => 400,
                "message" => $message,
                "params" => $params,
            ],
            400
        );

        parent::__construct($response, $message);
    }
}
