<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

/**
 * @author xgonzalez
 */

use Symfony\Component\HttpFoundation\JsonResponse;

class TODOException extends ResponseException
{

    public function __construct(string $message)
    {
        $response = new JsonResponse(
            [
                "status" => 501,
                "message" => $message,
            ],
            501
        );
        parent::__construct($response, $message);
    }

}
