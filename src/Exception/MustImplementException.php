<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Exception;

class MustImplementException extends \Exception
{

    public function __construct($instance, $class)
    {
        $instance = get_class($instance);
        parent::__construct("Class '$instance' must implement '$class' interface");
    }
}
