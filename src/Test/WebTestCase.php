<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Test;

/**
 * @author xgc1986
 */

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as SymfonyTestCase;

abstract class WebTestCase extends SymfonyTestCase
{

    protected $queue;

    public static function newClient()
    {
        $client = static::createClient();
        \Nakima\CoreBundle\Utils\Symfony::setKernel(self::$kernel);

        return $client;
    }
}
