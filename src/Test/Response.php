<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Test;

use Nakima\Utils\Object\JSON;

/**
 * @author xgc1986
 */
class Response
{

    protected $response;
    protected $valid;
    protected $testCase;
    protected $statusCode;

    public function __construct($response, $testCase)
    {
        $this->valid = true;
        $this->statusCode = $response->getStatusCode();

        $this->response = $response;
        $this->testCase = $testCase;
        $this->rawResponse = $response->getContent();

        if ($this->response instanceof \Symfony\Component\HttpFoundation\BinaryFileResponse) {
            $this->response = file_get_contents($response->getFile()->getPathName());
        } else {
            if ($this->response->headers->contains("Content-Type", "application/json")) {
                $this->response = JSON::decode($response->getContent(), false);
                $this->testCase->assertEquals(JSON_ERROR_NONE, JSON::lastError());
            } else {
                $this->response = $this->response->getContent();
            }
        }
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function print($level = 3)
    {
        \IO\Out::setLevel($level);
        if (!is_string($this->response)) {
            \IO\Out::var("\nresponse", $this->response, $level);
        } else {
            switch ($level) {
                case 0:
                    \IO\Out::info($this->response);
                    break;
                case 2:
                    \IO\Out::error($this->response);
                    break;
                case 1:
                default:
                    \IO\Out::warning($this->response);
                    break;
            }
        }
    }

    public function isValid()
    {
        return $this->valid;
    }

    public function getContent()
    {
        return $this->response;
    }

    public function getBody()
    {
        return $this->rawResponse;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function printHtmlError()
    {
        \IO\Out::enableDebug();
        \IO\Out::warning($this->response);

        return;
        $xml = new \DomDocument;
        $xml->loadHtml($this->response);
        foreach ($xml->getElementsByTagName('title') as $title) {
            echo "\n";
            \IO\Out::error("$title->nodeValue");
            break;
        }
        \IO\Out::dissableDebug();

        $xpath = new \DomXpath($xml);
        foreach ($xpath->query('//ol[@id="traces-0"]') as $rowNode) {
            foreach ($rowNode->childNodes as $row) {
                echo "    ";
                \IO\Out::warning(explode("\n", $row->nodeValue)[3]);
            }
        }

        \IO\Out::enableDebug();
    }
}
