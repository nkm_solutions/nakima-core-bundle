<?php
declare(strict_types=1);

namespace Nakima\CoreBundle\Test;

use Nakima\CoreBundle\Utils\Symfony;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @author xgc1986
 */
class Client
{

    protected $client;
    protected $testCase;
    protected $response;


    public static function create($testCase)
    {
        return new Client($testCase);
    }

    public function __construct($testCase)
    {
        $client = $testCase::newClient();
        $client->followRedirects();

        $this->setClient($client);
        $this->setTestCase($testCase);
    }


    public function setClient($client): void
    {
        $this->client = $client;
    }

    public function setTestCase($testCase): void
    {
        $this->testCase = $testCase;
    }

    public function getContainer(): ContainerInterface
    {
        $this->client->getContainer();
    }

    public function get($url, $params = [], $files = [], $headers = [], $extra = []): Response
    {
        return $this->request("GET", $url, $params, $files, $headers, $extra);
    }

    public function request($method, $url, $params = [], $files = [], $headers = [], $extra = []): Response
    {

        if ($url[0] !== "/") {
            $url = $this->getRoute($url, $params);
        }

        $uploads = [];
        foreach ($files as $key => $value) {
            $uploads[$key] = $this->createFile($value);
        }
        if (isset($headers["CONTENT_TYPE"]) && $headers["CONTENT_TYPE"] === 'application/json') {
            $this->client->request($method, $url, [], $uploads, $headers, json_encode($params));
        } else {
            $this->client->request($method, $url, $params, $uploads, $headers);
        }

        $this->response = new Response($this->client->getResponse(), $this->testCase);

        return $this->response;
    }

    public function getRoute($name, $params = [], $b = false): string
    {
        return Symfony::getContainer()->get('router')->generate($name, $params, $b);
    }

    public function createFile($file): UploadedFile
    {
        $file = Symfony::getRoot() . "/$file";
        $dest = tempnam(sys_get_temp_dir(), 'TEST');
        copy($file, $dest);

        return new UploadedFile($dest, "test_file");
    }

    public function post($url, $params = [], $files = [], $headers = [], $extra = []): Response
    {
        return $this->request("POST", $url, $params, $files, $headers, $extra);
    }

    public function put($url, $params = [], $files = [], $headers = [], $extra = []): Response
    {
        return $this->request("PUT", $url, $params, $files, $headers, $extra);
    }

    public function delete($url, $params = [], $files = [], $headers = [], $extra = []): Response
    {
        return $this->request("DELETE", $url, $params, $files, $headers, $extra);
    }

    public function check200(): void
    {
        $this->check(200);
    }

    protected function check($status): void
    {
        $level = \IO\Out::getLevel();
        \IO\Out::setLevel(3);
        $this->testCase->assertNotNull($this->response);
        if ($this->response->getStatusCode() === 500) {
            $this->response->printHtmlError();
        } else {
            if ($this->response->getStatusCode() !== $status) {
                $this->response->print();
            }
        }

        $this->testCase->assertEquals($status, $this->response->getStatusCode());

        \IO\Out::setLevel($level);
    }

    public function check400(): void
    {
        $this->check(400);
    }

    public function check401(): void
    {
        $this->check(401);
    }

    public function check403(): void
    {
        $this->check(403);
    }

    public function check404(): void
    {
        $this->check(404);
    }

    public function check405(): void
    {
        $this->check(405);
    }

    public function getResponse(): Response
    {
        return $this->response;
    }

    public function getContent()
    {
        return $this->response->getContent();
    }


    public function printResponse(): void
    {
        $this->response->print();
    }

    public function createFileParam($folder, $newFile): UploadedFile
    {

        trigger_error(
            __METHOD__." is deprecated, it will be removed in the stable version, use ".__CLASS__."::createFile instead",
            E_USER_DEPRECATED
        );

        $folder = getcwd()."/".$folder;
        $file = $folder."/".$newFile;

        system("cp -f $file /tmp/$newFile");

        return new UploadedFile(
            "/tmp/$newFile",
            $newFile
        );
    }
}
