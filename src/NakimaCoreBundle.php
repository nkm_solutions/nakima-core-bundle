<?php
declare(strict_types=1);
namespace Nakima\CoreBundle;

/**
 * @author xgonzalez@nakima.es
 */

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NakimaCoreBundle extends Bundle
{
}
