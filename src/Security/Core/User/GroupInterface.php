<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Security\Core\User;

/**
 * @author xgonzalez@nakima.es
 */
interface GroupInterface
{

    public function getGroups();

    public function hasGroup($group);
}
