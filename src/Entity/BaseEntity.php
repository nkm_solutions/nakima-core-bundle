<?php
declare(strict_types=1);

namespace Nakima\CoreBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Doctrine\ORM\Mapping\PostPersist;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @HasLifecycleCallbacks
 * @MappedSuperclass
 */
class BaseEntity implements IEntity
{

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __get($field)
    {
        return $this->{$field};
    }

    public function __has($field)
    {
        return isset($this->{$field});
    }

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
        ];
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
    }

    public function __toString()
    {
        return "id: $this->id";
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        if (!$id) {
            return $this;
        }
        $this->id = $id;

        return $this;
    }

    /*public function __get($param) {
        if ("self_" === substr($param, 0, 5)) {
            return $this;
        }
    }*/

    public function get($field)
    {
        $ret = $this->{"get".ucfirst($field)}();

        /*if ($ret instanceof \Doctrine\ORM\PersistentCollection) {
            return "Collection";
        }

        if ($ret instanceof \Doctrine\Common\Collections\ArrayCollection) {
            return "Collection";
        }*/

        return $ret;
    }

    public function __set($field, $value)
    {
        if ($field == 'id') {
            return $this->setId($value);
        }
        $this->{$field} = $value;

        return $this;
    }

    // TODO

    /**
     * @PostPersist
     */
    public function __postPersist()
    {
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        $this->__validate($context, $payload);
    }

    public function __validate($context, $payload)
    {
    }
}
