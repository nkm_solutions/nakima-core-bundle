<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\Utils\String\Text;
use Nakima\Utils\Time\DateTime;

/**
 * @MappedSuperclass
 */
abstract class BaseMetadataEntity extends BaseEntity
{

    /**
     * @Column(type="json")
     */
    protected $metadata;

    /**
     * @Column(type="nakima_datetime")
     */
    protected $createdAt;

    /**
     * @Column(type="string", length=64, unique=true)
     */
    protected $ref;

    /**************************************************************************
     *                                                                        *
     *   Custom Functions                                                     *
     *                                                                        *
     **************************************************************************/

    public function __construct()
    {
        $this->metadata = [];
        $this->setCreatedAt(new DateTime);
    }

    public function getMeta($key)
    {
        return $this->metadata[$key];
    }

    public function optMeta($key, $default)
    {
        return $this->metadata[$key] ?? $default;
    }

    public function addMeta($key, $value)
    {
        $this->metadata[$key] = Doctrine::getId($value);

        return $this;
    }

    public static function create()
    {
        $class = get_called_class();
        $metadata = new $class();

        $baseRef = Text::rstr(16);
        $i = 0;

        do {
            $ref = $baseRef.$i;
            $i++;
        } while (self::load($ref));

        $metadata->setRef($ref);

        return $metadata;
    }

    public static function load($ref)
    {

        $class = get_called_class();
        $splittedClass = explode('\\', $class);
        $doctrine = Doctrine::get();

        $a = $splittedClass[0];
        $b = $splittedClass[count($splittedClass) - 1];

        $ret = $doctrine->getRepository("$a:$b")->findOneByRef($ref);

        return $ret;
    }

    /**************************************************************************
     *                                                                        *
     *   Getters & Setters                                                    *
     *                                                                        *
     **************************************************************************/

    public function getRef()
    {
        return $this->ref;
    }

    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    public function getMetadata()
    {
        return $this->metadata;
    }

    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
