<?php
declare(strict_types=1);

namespace Nakima\CoreBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Nakima\CoreBundle\Utils\Doctrine;

/**
 * @MappedSuperclass
 */
abstract class BaseStatusEntity extends BaseEntity
{

    /**
     * @Column(type="string", length=64, unique=true)
     */
    protected $name;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __construct(string $name = null)
    {
        $this->name = $name;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }

    public static function load(string $name): ?BaseStatusEntity
    {

        $class = get_called_class();
        $splittedClass = explode('\\', $class);
        $doctrine = Doctrine::get();

        $a = $splittedClass[0];
        $b = $splittedClass[count($splittedClass) - 1];

        return $doctrine->getRepository("$a:$b")->findOneByName($name);
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

}
