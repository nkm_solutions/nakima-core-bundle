<?php
declare(strict_types=1);

namespace Nakima\CoreBundle\Entity;

interface IEntity
{
    public function __toArray(array $options = []): array;
}
