<?php
declare(strict_types=1);

namespace Nakima\CoreBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Doctrine\ORM\Mapping\PrePersist;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\Utils\String\Text;

/**
 * @MappedSuperclass
 * @HasLifecycleCallbacks
 */
abstract class BaseStatusSlugEntity extends BaseEntity
{

    /**
     * @Column(type="string")
     */
    protected $name;

    /**
     * @Column(type="string", unique=true)
     */
    protected $slug;


    /******************************************************************************************************************
     *                                                                                                                *
     * Custom Functions                                                                                               *
     *                                                                                                                *
     ******************************************************************************************************************/

    public function __construct($name = null)
    {
        $this->name = $name;
    }

    public static function load($name): ?BaseStatusSlugEntity
    {
        $class = get_called_class();
        $splittedClass = explode('\\', $class);
        $doctrine = Doctrine::get();

        $a = $splittedClass[0];
        $b = $splittedClass[count($splittedClass) - 1];

        return $doctrine->getRepository("$a:$b")->findOneByName($name);
    }

    public function __toString()
    {
        return $this->name;
    }

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'slug' => $this->getSlug(),
        ];
    }

    /******************************************************************************************************************
     *                                                                                                                *
     * Getters & Setters                                                                                              *
     *                                                                                                                *
     ******************************************************************************************************************/

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        $this->setSlug($name);

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        if ($slug !== null) {
            $this->slug = strtolower(Text::slugify($slug));
        }

        return $this;
    }

    /**
     * @PrePersist()
     */
    public function preCreate()
    {
        $class = get_called_class();
        $splittedClass = explode('\\', $class);
        $doctrine = Doctrine::get();

        $a = $splittedClass[0];
        $b = $splittedClass[count($splittedClass) - 1];

        $reds = $doctrine->getRepository("$a:$b")->findByName($this->getName());
        $slug = strtolower(Text::slugify($this->getName()));

        if (count($reds) >= 1) {
            $slug .= ("_".(count($reds)));
        }

        $this->setSlug($slug);
    }
}
