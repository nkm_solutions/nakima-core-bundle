<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Type;

/**
 * @author Javier Gonzalez Cuadrado (xgc1986@gmail.com)
 */

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

class IpType extends Type
{

    public function getName()
    {
        return 'ip';
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "VARCHAR(15)";
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {

        $matches = [];
        preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/', $value, $matches);

        if (!count($matches)) {
            throw ConversionException::conversionFailedFormat($value, $this->getName());
        }

        $matches = explode(".", $value);
        foreach ($matches as $key => $value) {
            if ($value > "255") {
                throw ConversionException::conversionFailedFormat($value, $this->getName());
            }
        }

        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }
}
