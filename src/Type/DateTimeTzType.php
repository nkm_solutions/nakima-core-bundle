<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Type;

/**
 * @author Javier Gonzalez Cuadrado (xgc1986@gmail.com)
 */

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use Nakima\Utils\Time\DateTime;

class DateTimeTzType extends Type
{

    public function getName()
    {
        return 'nakima_datetimetz';
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getDateTimeTzTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return ($value !== null)
            ? $value->format($platform->getDateTimeTzFormatString()) : null;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value instanceof DateTime) {
            return $value;
        }

        $val = DateTime::fromFormat($platform->getDateTimeTzFormatString(), $value);

        if (!$val) {
            $val = new DateTime(date_create($value));
        }

        if (!$val) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                $platform->getDateTimeTzFormatString()
            );
        }

        return $val;
    }
}
