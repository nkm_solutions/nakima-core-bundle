<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Type;

/**
 * @author Javier Gonzalez Cuadrado (xgc1986@gmail.com)
 */

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use Nakima\Utils\Object\JSON;

class JsonType extends Type
{

    public function getName()
    {
        return 'json';
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "TEXT";
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        $ret = JSON::encode($value);

        if ($ret === null) {
            throw ConversionException::conversionFailedFormat($value, $this->getName(), JSON::lastErrorMessage());
        }

        return $ret;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return $value;
        }

        $val = JSON::decode($value);

        if ($val === null) {
            throw ConversionException::conversionFailedFormat($value, $this->getName(), JSON::lastErrorMessage());
        }

        return $val;
    }
}
