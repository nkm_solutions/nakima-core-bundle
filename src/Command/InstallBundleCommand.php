<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Command;

/**
 * @author Javier Gonzalez Cuadrado < xgc1986@gmail.com >
 */

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InstallBundleCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this
            ->setName('nakima:bundle:install')
            ->setDescription('Install this bundle')
            ->setHelp("Yes, install this bundle")
            ->addArgument('bundle', InputArgument::REQUIRED, 'The bundle name');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $bundle = $input->getArgument('bundle');
        $bundleName = ucfirst($bundle."Bundle");

        $root = $this->getContainer()->getParameter('kernel.root_dir');
        $src = "$root/../vendor/vivi/nakima-$bundle-bundle/bundle";
        $dest = "$root/../src/$bundleName";

        if (file_exists($src)) {

            $this->writeInfo($output, "Installing Nakima$bundleName...");
            $this->recurse_copy($src, $dest);
            $output->writeln(
                "Installation complete\nRemember to enable <comment>new $bundleName()</comment> in AppKernel.\n"
            );

        } else {
            $this->writeError($output, "file '$src' does not exist.");
        }
    }

    private function getBundleNameFromClass($class)
    {
        $application = $this->getApplication();
        /* @var $application Application */

        foreach ($application->getKernel()->getBundles() as $bundle) {
            if (strpos($class, $bundle->getNamespace().'\\') === 0) {
                return $bundle->getName();
            }
        }

        return;
    }

    private function getBundle($name)
    {
        return $this->getKernel()->getBundle($name);
    }

    private function getKernel()
    {
        $application = $this->getApplication();

        return $application->getKernel();
    }

    private function writeError(OutputInterface $output, $message)
    {
        $output->write(sprintf("\n<error> %s </error>", $this->getEmptyLine(strlen($message))));
        $output->write(sprintf("\n<error> %s </error>", $message));
        $output->write(sprintf("\n<error> %s </error>", $this->getEmptyLine(strlen($message))));
        $output->writeln(sprintf("\n<error></error>"));
    }

    private function writeInfo(OutputInterface $output, $message)
    {
        $output->write(sprintf("\n<info> %s </info>", $this->getEmptyLine(strlen($message))));
        $output->write(sprintf("\n<info> %s </info>", $message));
        $output->write(sprintf("\n<info> %s </info>", $this->getEmptyLine(strlen($message))));
        $output->writeln(sprintf("\n<info></info>"));
    }

    private function writeComment(OutputInterface $output, $message)
    {
        $output->write(sprintf("\n<comment> %s </comment>", $this->getEmptyLine(strlen($message))));
        $output->write(sprintf("\n<comment> %s </comment>", $message));
        $output->write(sprintf("\n<comment> %s </comment>", $this->getEmptyLine(strlen($message))));
        $output->writeln(sprintf("\n<comment></comment>"));
    }

    private function getEmptyLine($length)
    {
        $ret = "";

        for ($i = 0; $i < $length; $i++) {
            $ret .= ' ';
        }

        return $ret;
    }

    private function recurse_copy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src.'/'.$file)) {
                    $this->recurse_copy($src.'/'.$file, $dst.'/'.$file);
                } else {
                    copy($src.'/'.$file, $dst.'/'.$file);
                }
            }
        }
        closedir($dir);
    }
}