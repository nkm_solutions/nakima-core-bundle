<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\OptionsResolver\Exception\InvalidArgumentException;

abstract class AbstractCommand extends ContainerAwareCommand
{
    protected $input;
    protected $output;
    protected $helper;

    /**
     * @see ContainerAwareCommand configure
     */
    public function configure()
    {
    }

    /**
     * The same as execute but without params, you can access them like class attributes
     */
    abstract public function realize();

    /**
     * @see ContainerAwareCommand execute
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $this->helper = $this->getHelper('question');
        $this->realize();
    }

    /**
     * An alias for $this->helper->ask
     */
    protected function ask($question)
    {
        if ($this->output) {
            $ret = "";
            $question->setMaxAttempts(3);

            return $this->helper->ask($this->input, $this->output, $question);
        }
    }

    /**
     * An alias for $this->output->write
     */
    protected function write($message)
    {
        if ($this->output) {
            $this->output->write($message);
        }
    }

    /**
     * An alias for $this->output->writeln
     */
    protected function writeln($message)
    {
        if ($this->output) {
            $this->output->writeln($message);
        }
    }

    /**
     * Reads a String from input, you have 3 a tries before it throws an exception
     */
    protected function readString($message, $cb = null)
    {
        $question = new Question($message);
        if ($cb) {
            $question->setValidator($cb);
        } else {
            $question->setValidator(
                function ($value) {
                    if (trim($value) == '') {
                        throw new InvalidArgumentException('You must insert a value');
                    }

                    return $value;
                }
            );
        }

        return $this->ask($question);
    }

    /**
     * Reads a Url from input, you have 3 a tries before it throws an exception
     */
    protected function readUrl($message, $cb = null)
    {
        $question = new Question($message);
        if ($cb) {
            $question->setValidator($cb);
        } else {
            $question->setValidator(
                function ($value) {
                    $valid = filter_var($value, FILTER_VALIDATE_URL);
                    if (!$valid) {
                        throw new InvalidArgumentException("You must insert a valid URL");
                    }

                    return $value;
                }
            );
        }

        return $this->ask($question);
    }

    /**
     * Given a list of items, the input expects to read a key of an element of the list,
     * you have 3 a tries before it throws an exception
     */
    protected function readSelection($message, $list, $cb = null)
    {
        $ret = "";
        $this->write($message);
        foreach ($list as $key => $value) {
            $number = $key;
            $stringValue = $value;
            if (is_object($value)) {
                $stringValue = $value->__toString();
            }
            $this->output->write("  [$number] $stringValue\n");
        }
        $question = new Question("Choose one: ");
        if ($cb) {
            $question->setValidator($cb);
        } else {
            $question->setValidator(
                function ($value) use ($list) {
                    $valid = array_key_exists($value, $list);
                    if (!$valid) {
                        throw new InvalidArgumentException("Invalid input");
                    }

                    return $list[$value];
                }
            );
        }

        return $this->ask($question);
    }

    public function isEnv($env)
    {
        return $this->getContainer()->getParameter('kernel.environment') === $env;
    }

    public function isEnvDev()
    {
        return $this->isEnv('dev');
    }

    public function isEnvProd()
    {
        return $this->isEnv('prod');
    }

    public function isEnvTest()
    {
        return $this->isEnv('test');
    }

    public function getArgument($name)
    {
        return $this->input->getArgument($name);
    }
}