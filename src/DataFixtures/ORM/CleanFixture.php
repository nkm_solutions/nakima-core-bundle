<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\DataFixtures\ORM;

/**
 * @author xgonzalez@nakima.es
 */


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nakima\CoreBundle\Entity\IEntity;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class CleanFixture extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private static $map = [];
    private        $container;
    private        $manage;

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    public function getOrder(): int
    {
        return 1;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        if ($this->isEnvProd()) {
            $this->loadProd();
        }

        if ($this->isEnvDev()) {
            $this->loadDev();
        }

        if ($this->isEnvTest()) {
            $this->loadTest();
        }

        $this->manager->flush();
    }

    public function isEnvProd(): bool
    {
        return "prod" == $this->getEnv();
    }

    public function getEnv(): string
    {
        return $this->container->getParameter('kernel.environment');
    }

    public function loadProd(): void
    {
    }

    public function isEnvDev(): bool
    {
        return "dev" == $this->getEnv();
    }

    public function loadDev(): void
    {
    }

    public function isEnvTest(): bool
    {
        return "test" == $this->getEnv();
    }

    public function loadTest(): void
    {
    }

    public function getRandom(string $clzz): IEntity
    {
        return $this->get(self::$map[$clzz][rand(0, count(self::$map[$clzz]) - 1)]);
    }

    public function get(string $key)
    {
        return $this->getReference($key);
    }

    public function persist(IEntity $entity, string $key = null): void
    {
        $this->manager->persist($entity);

        if ($key) {
            $this->put($key, $entity);

            $clzz               = get_class($entity);
            self::$map[$clzz]   = self::$map[$clzz] ?? [];
            self::$map[$clzz][] = $key;
        }

    }

    public function put(string $key, IEntity $value)
    {
        return $this->addReference($key, $value);
    }

    public function flush(): void
    {
        $this->manager->flush();
    }
}
