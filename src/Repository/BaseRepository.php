<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Repository;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\ORM\EntityRepository;

abstract class BaseRepository extends EntityRepository
{

    /*
    protected abstract function transform($entity, $translator = null, $format = 'json');

    public function transformEntity($entity, $translator = null, $format = 'json') {
        if (!$entity) {
            return null;
        }

        if (
            $entity instanceof ArrayCollection or
            $entity instanceof PersistentCollection or
            is_array($entity)
        ) {

            $ret = array();

            foreach ($entity as $key => $value) {
                $ret[] = $this->transform($value, $translator, $format);
            }

            return $ret;
        } else {
            return $this->transform($entity, $translator, $format);
        }
    }
    */

    protected function getRepo($fqn)
    {
        return $this->getEntityManager()
            ->getRepository($fqn);
    }

    public function getContainer()
    {
        global $kernel;
        if ($kernel instanceOf \AppCache) {
            $kernel = $kernel->getKernel();
        }

        return $kernel->getContainer();
    }

    public function getUser()
    {

        if ($this->getContainer()->get('security.token_storage')->getToken()) {
            return $this->getContainer()->get('security.token_storage')->getToken()->getUser();
        }

        return null;
    }
}