<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Mailer;

use Nakima\CoreBundle\Utils\Symfony;

class Mailer
{

    private static $maps = [];
    private $message;

    public static function newInstance(
        string $subject,
        string $from,
        string $to,
        string $template,
        array $params = []
    ): Mailer {
        $params['message'] = $params['message'] ?? '';
        $params['user'] = $params['user']    ?? Symfony::getUser();
        $mail = new Mailer($subject, $from, $to, $template, $params);
        self::$maps[] = $mail;

        return $mail;
    }

    public static function send()
    {

        $mailer = Symfony::get('mailer');

        foreach (self::$maps as $key => $value) {
            ($mailer->send($value->getMessage()));
        }

        self::$maps = [];
    }

    private function __construct($subject, $from, $to, $template, array $params = [])
    {

        $params['user'] = $params['user'] ?? Symfony::getUser();
        $params['subject'] = $params['subject'] ?? $subject;
        $params['from'] = $params['from'] ?? $from;
        $params['to'] = $params['to'] ?? $to;
        $params['template'] = $params['template'] ?? $template;

        $this->message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody(Symfony::get('twig')->render($template, $params), 'text/html');
    }

    public function getMessage()
    {
        return $this->message;
    }
}