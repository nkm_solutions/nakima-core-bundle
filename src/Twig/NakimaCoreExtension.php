<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Twig;

use Nakima\CoreBundle\Utils\Symfony;

/**
 * @author xgc1986 < xgonzalez@nakima.es >
 *
 */
class NakimaCoreExtension extends \Twig_Extension
{

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return [
            '_nakima' => new \Twig_SimpleFunction('_nakima', array($this, '_nakima')),
            '_load' => new \Twig_SimpleFunction('_load', array($this, '_load')),
            '_loadBlock' => new \Twig_SimpleFunction('_loadBlock', array($this, '_loadBlock')),
            '_getBundleName' => new \Twig_SimpleFunction('_getBundleName', array($this, '_getBundleName')),
            '_getEntityName' => new \Twig_SimpleFunction('_getEntityName', array($this, '_getEntityName')),
            '_isBaseEntity' => new \Twig_SimpleFunction('_isBaseEntity', array($this, '_isBaseEntity')),
            '_getControllerName' => new \Twig_SimpleFunction('_getControllerName', array($this, '_getControllerName')),
            '_maximumUploadSize' => new \Twig_SimpleFunction('_maximumUploadSize', array($this, '_maximumUploadSize')),
        ];

    }

    public function _nakima($param)
    {
        return $this->container->getParameter($param);
    }


    public function _load($class)
    {
        return new $class;
    }

    private static $blockMap = [];

    public function _loadBlock($bundle, $entity)
    {
        $blocks = Symfony::getContainer()->getParameter('nakima_admin')["blocks"];
        $key = "$bundle.$entity";
        self::$blockMap[$key] = self::$blockMap[$key] ?? false;

        if (!self::$blockMap[$key]) {
            $class = $bundle."Bundle\\Entity\\$entity";
            $this->entityFqn = $bundle."Bundle:$entity";
            $configBlock = false;
            foreach ($blocks as $key => $block) {
                if (isset($block["meta"]) && isset($block["meta"]["type"]) && $block["meta"]["type"] == "entity") {
                    if ($block["meta"]["entity"] == $class) {
                        self::$blockMap[$key] = $block;
                        break;
                    }
                }
            }
        }

        return self::$blockMap[$key] ?? false;
    }

    private static $bundleMap = [];

    public function _getBundleName($entity)
    {
        $class = get_class($entity);

        self::$bundleMap[$class] = self::$bundleMap[$class] ?? false;
        if (!self::$bundleMap[$class]) {
            $matches = [];

            preg_match("/Proxies__CG__(\w+)Bundle/", str_replace("\\", '', $class), $matches);

            if (!count($matches) >= 1) {
                preg_match("/(\w+)Bundle/", str_replace("\\", '', $class), $matches);
            }

            self::$bundleMap[$class] = $matches[1];
        }

        return self::$bundleMap[$class];
    }

    private static $entityMap = [];

    public function _getEntityName($entity)
    {
        $class = get_class($entity);

        self::$entityMap[$class] = self::$entityMap[$class] ?? false;
        if (!self::$entityMap[$class]) {
            $matches = [];
            preg_match("/\\w+$/", $class, $matches);
            self::$entityMap[$class] = $matches[0];
        }

        return self::$entityMap[$class];
    }

    public function _isBaseEntity($class)
    {
        return $class instanceof \Nakima\CoreBundle\Entity\BaseEntity;
    }

    private static $controllerMap = [];

    public function _getControllerName($controller)
    {

        $class = $controller;

        self::$controllerMap[$class] = self::$controllerMap[$class] ?? false;
        if (!self::$controllerMap[$class]) {
            $matches = [];
            preg_match("/(\\w+)Controller::\\w+Action$/", $controller, $matches);
            self::$controllerMap[$class] = $matches[0];

            if (count($matches) > 1) {
                self::$controllerMap[$class] = strtolower($matches[1]);
            } else {
                self::$controllerMap[$class] = "homecontroller";
            }
        }

        return self::$controllerMap[$class];
    }


    public function getName()
    {
        return 'nakima_core_extension';
    }

    public function _maximumUploadSize()
    {
        return \Nakima\Utils\Settings\PHP::maxFileSize();
    }
}
