<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Form;

/**
 * @author xgonzalez@nakima.es
 */

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractBaseType extends AbstractType
{

    abstract public function getDefaultOptions();

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults($this->getDefaultOptions());
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        foreach ($this->getDefaultOptions() as $key => $option) {

            /*if ($option === null) {
                // TODO create custom exception
                throw new \Exception("$key option is required in " . get_class($this));
            }*/

            $view->vars[$key] = $options[$key];
        }
    }
}
