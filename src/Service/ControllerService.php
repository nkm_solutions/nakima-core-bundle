<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Service;

/**
 * @author xgonzalez
 */

use Nakima\CoreBundle\Exception\ResponseException;
use Nakima\CoreBundle\Utils\Symfony;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ControllerService implements EventSubscriberInterface
{

    public function onKernelRequest(GetResponseForExceptionEvent $event)
    {
        $request = $event->getRequest();
        $exception = $event->getException();

        if ($exception instanceof ResponseException) {
            $event->setResponse($exception->getResponse());
            $container = Symfony::getContainer();

            if ($container->has("nakima.log") && strlen($request->getRequestUri())  > 1 && $request->getRequestUri()[1] !== "_") {
                $container->get("nakima.log")->e(
                    "[{$exception->getStatus()}] {$request->getMethod()} {$request->getSchemeAndHttpHost()}{$request->getRequestUri()}"
                );
            }
        } else {
            $container = Symfony::getContainer();

            if ($container->has("nakima.log") && strlen($request->getRequestUri()) > 1 && $request->getRequestUri()[1] !== "_") {
                $container->get("nakima.log")->e(
                    "[500] {$request->getMethod()} {$request->getSchemeAndHttpHost()}{$request->getRequestUri()}"
                );
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [['onKernelRequest', 17]],
        ];
    }
}
