<?php
declare(strict_types=1);
namespace Nakima\CoreBundle\Service;

/**
 * @author xgonzalez
 */

use Nakima\CoreBundle\Utils\Symfony;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestService implements EventSubscriberInterface
{

    public function onKernelRequest(FilterControllerEvent $event)
    {

        $request = $event->getRequest();
        $container = Symfony::getContainer();
        if ($container->has("nakima.log") && strlen($request->getRequestUri()) > 1 && $request->getRequestUri()[1] !== "_") {
            $container->get('nakima.log')->i(
                "{$request->getMethod()} {$request->getSchemeAndHttpHost()}{$request->getRequestUri()}"
            );
        }

        if ($request->headers->get('Content-Type') === "application/json") {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());

        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => array(array('onKernelRequest', 17)),
        );
    }
}
