<?php

/**
 * @author jgonzalez@nakima.es
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Exception\MethodException;

use PHPUnit\Framework\TestCase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class BaseControllerTest extends TestCase {

	/**
	 * @test
	 * @expectedException     \Nakima\CoreBundle\Exception\MethodException
	 */
	public function testCheckMethod() {

		$controller = new DummyCotroller();

		$request = Request::create('/');
		$request->setLocale('en');
		$request->setRequestFormat('xml');

		$requestStack = new RequestStack();
        $requestStack->push($request);

        $kernel = $this->createMock('Symfony\Component\HttpKernel\HttpKernelInterface');
        $kernel->expects($this->once())->method('handle')->will($this->returnCallback(function (Request $request) use ($controller) {}));
        $container = $this->createMock('Symfony\Component\DependencyInjection\ContainerInterface');
        $container->expects($this->at(0))->method('get')->will($this->returnValue($requestStack));
        $container->expects($this->at(1))->method('get')->will($this->returnValue($kernel));

        $controller->setContainer($container);
        $response = $controller->forward('a_controller');
        $response = $controller->setRequest($request);
        $response = $controller->checkMethod('POST');
        //$this->assertEquals('xml--fr', $response->getContent());
	}
}

class DummyCotroller extends BaseController {

	public function forward($controller, array $path = array(), array $query = array()) {
        return parent::forward($controller, $path, $query);
    }
}